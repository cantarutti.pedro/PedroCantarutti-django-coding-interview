from django.db import models
from django.contrib.auth.models import User


class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    enrollment_completed = models.BooleanField(default=False)
    student_id = models.IntegerField(unique=True)

    def __str__(self) -> str:
        return self.user.username