from django.test import TestCase
from rest_framework.test import APIRequestFactory

from .views import StudentViewSet

class StudentViewSetTestCase(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.view = StudentViewSet.as_view(actions=StudentViewSet.action_map)

    def test_list_students(self):
        request = self.factory.get('/students/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
