from rest_framework.viewsets import ModelViewSet

from .serializers import StudentSerializer
from .models import Student


class StudentViewSet(ModelViewSet):
    action_map = {
        'get': 'list',
        'post': 'create',
        'put': 'update',
    }

    queryset = Student.objects.all()
    serializer_class = StudentSerializer
