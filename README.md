# BEON

## _Python/Django Coding Exercise_

### Requirements

- PostgresDB
  - Configure it in the settings

### Running env

1. Execute migrations

```bash
  ./manage.py migrate
```

2. create the first user

```bash
  ./manage.py createsuperuser
```

3. Start the server

```bash
  ./manage.py runserver
```

4. Run tests

```bash
  ./manage.py test
```

* A more complete version of this coding assignment can be found within the dev branch
